﻿

**CrazyNumbers**

Con el fin de que cada parte del proyecto funcionara de forma modular, se organizó la estructura

buscando que cada funcionalidad tuviera funcionamiento desacoplable sin dependencia de

ninguna otra clase (exceptuando los controladores que contienen la dependencia de los scripts

que controlan). Una vez creado cada módulo / controlador se creo el flujo de funcionamiento por

medio del GameManager.

Diagrama de clases de la estructura del código:

[Click](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[ ](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[aquí](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[ ](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[para](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[ ](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[ver](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[ ](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[imagen](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[ ](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[en](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[ ](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)[drive](https://drive.google.com/file/d/1cEQV89EHHZF50fruuZQckMzGs-Si506u/view?usp=sharing)

Descripción de cada módulo:

\-

**NumbersController:**

Este script es el encargado de cada número con sus características (valor en int y en

string), a través del método de “SetNumberList” se puede crear configurar la lista

automáticamente siempre y cuando en el apartado de “Enums” este descrito el valor de

cada número, la cantidad a crearse dependerá del calor definido en editor:

de lo contraria se puede hacer de formal manual agregando cada valor en el editor, para

lograr esto basta con volver serializada la variable de “Numbers”





Por último, en caso de que se quiera crear un nuevo numero sin necesidad de depender

del controlador, se puede hacer con “new Number(NumberType)”, pues este cuenta con

un constructor para ello.

\-

\-

**NoticeController:**

Este script se encarga de mostrar en pantalla, de forma escrita (string), un número, este

número se obtiene previamente desde el “GameManager”, quien es el que se lo pide a

“NumbersController”. Una vez mostrado el número, se llama un evento que indica que

termino de hacerlo, este evento se creo con el fin de que, según el flujo escrito en el

“GameManager”, se subscriba el siguiente modulo a ejecutar consecutivamente.

**OptionController:**

Este script es el encargado de mostrar opciones de repuesta en la pantalla, estos valores

se dan de forma random, cada opción de respuesta tiene un bool que indica que es la

opción errónea exceptuando a la opción de numero obtenida desde “NumberController”.

El valor lo configura el “GameManger” una vez se lo pide a NumberController.

Este script al igual que los demás módulos, tienen un evento que se ejecuta según el botón

presionado, una vez finalizada la acción a este evento se subscribe el módulo siguiente a

ejecutar.

\-

**FallingObjectsController**

Este pequeño extra fue creado para agregarle un poco de dinamismo a lo requerido en la

prueba. La finalidad de este script es, por medio de un pool, crear una cantidad de objetos

que caen (la cantidad se determina desde “NumbersController” y es entregada por

“GameManager”) para que el usuario los cuente, esto se hace previo a que

“NoticeController” muestre el valor y al igual que los demás módulos, una vez finalizada la

acción se llama un evento para que se subscriba el módulo siguiente a ejecutar

**Haz click en los siguientes enunciados para ver información extra:**

[Documentación](https://drive.google.com/drive/folders/1_hVQifSrNFrmHJMAWPgDfBgefeD5l9vT?usp=sharing)[ ](https://drive.google.com/drive/folders/1_hVQifSrNFrmHJMAWPgDfBgefeD5l9vT?usp=sharing)[completa](https://drive.google.com/drive/folders/1_hVQifSrNFrmHJMAWPgDfBgefeD5l9vT?usp=sharing)

[Compilados](https://drive.google.com/drive/folders/1-XwZDJotOZgdxdEHUmYih-smTBPm573B?usp=sharing)

[Juego](https://simmer.io/@Thokora/crazy-numbers)[ ](https://simmer.io/@Thokora/crazy-numbers)[en](https://simmer.io/@Thokora/crazy-numbers)[ ](https://simmer.io/@Thokora/crazy-numbers)[web](https://simmer.io/@Thokora/crazy-numbers)

