﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    [SerializeField] private float timeTogameLoop = 2f;
    [SerializeField] private TutorialBehaviour tutorialBehaviour;
    [Space]
    [SerializeField] private FallingObjectsController fallingObjectsController;
    [SerializeField] private NoticeController noticeController;
    [SerializeField] private OptionsController optionsController;
    [SerializeField] private ScoreController scoreController;

    private Number correctNumber = null;

    void Start()
    {
        Initialization();
    }

    private void Initialization()
    {
        optionsController.SetOptions();
        fallingObjectsController.Initialization();
        tutorialBehaviour.StartGame += StartGameplay;
        Instructions();
    }

    private void Instructions()
    {
        tutorialBehaviour.ShowInstructionsPanel();
    }

    private void StartGameplay()
    {
        tutorialBehaviour.StartGame -= StartGameplay;

        Globals.GameStarted = true;
        scoreController.ShowScorePanel();

        optionsController.CorrectResponse += OnCorrectTask;
        optionsController.WrongResponse += OnWrongTask;

        StartCoroutine(GameLoop());
    }

    private void OnCorrectTask()
    {
        scoreController.AddCorrectScore();
        StartCoroutine(GameLoop());
    }

    private void OnWrongTask()
    {
        scoreController.AddWrongScore();
        StartCoroutine(GameLoop());
    }

    IEnumerator GameLoop()
    {
        correctNumber = NumbersManager.GetRandomNumber(correctNumber);

        fallingObjectsController.ObjectsDisplayed += OnObjectsDisplayed;

        yield return new WaitForSeconds(timeTogameLoop);

        fallingObjectsController.StartFallObjects(correctNumber.GetNumberValue());
    }

    private void OnObjectsDisplayed()
    {
        fallingObjectsController.ObjectsDisplayed -= OnNoticeDisplayed;

        noticeController.NoticeDisplayed += OnNoticeDisplayed;
        noticeController.SetNoticeNumber(correctNumber.GetNumberName(), timeTogameLoop);
    }

    private void OnNoticeDisplayed()
    {
        noticeController.NoticeDisplayed -= OnNoticeDisplayed;
        optionsController.OptionRandomizer(correctNumber);
    }

    private void EndGameplay()
    {
        optionsController.CorrectResponse -= OnCorrectTask;
        optionsController.WrongResponse -= OnWrongTask;
        scoreController.HideScorePanel();
        Globals.GameStarted = false;
    }
}
