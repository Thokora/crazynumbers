using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FallingObjectsController : MonoBehaviour
{
    public Action ObjectsDisplayed;

    [SerializeField] private GameObject attentionElementsContainer;
    [SerializeField] private AttentionElementsBehaviour attentionElementsBehaviour;

    [Header("Pool settings")]
    [SerializeField] private PoolAttentionObjects poolAttentionObjects;
    [SerializeField] private int maxAttentionObjectPerType = 2;
    [SerializeField] private int pooledAmount = 20;
    [SerializeField] private float showTime = 5f;

    private int totalAttentionObjects = 0;

    public void Initialization()
    {
        attentionElementsBehaviour.SetGameDurationTime(showTime);

        poolAttentionObjects.Initialize(maxAttentionObjectPerType, pooledAmount);        
        poolAttentionObjects.DisableAllPooledObjs();
        attentionElementsContainer.SetActive(false);
    }

    public void StartFallObjects(int objectsAmount)
    {
        attentionElementsContainer.SetActive(true);
        totalAttentionObjects = objectsAmount;
        attentionElementsBehaviour.TimeFinished += OnEndTimeToShow;
        gameObject.SetActive(true);
        poolAttentionObjects.DisableAllPooledObjs();
        attentionElementsBehaviour.SetRandomAttentionElements(totalAttentionObjects, maxAttentionObjectPerType, poolAttentionObjects);
    }

    private void OnEndTimeToShow()
    {
        attentionElementsBehaviour.TimeFinished -= OnEndTimeToShow;
        ObjectsDisplayed?.Invoke();
        attentionElementsContainer.SetActive(false);
    }
}
