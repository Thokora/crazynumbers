﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreController : MonoBehaviour
{
    [SerializeField] GameObject scorePanel;
    [SerializeField] private TMP_Text correctTaskText;
    [SerializeField] private TMP_Text wrongTaskText;

    private int correctTasks;
    private int wrongTasks;

    private void Awake()
    {
        HideScorePanel();
    }

    public void AddCorrectScore()
    {
        correctTasks++;
        correctTaskText.text = "ACIERTOS: " + correctTasks.ToString();
    }

    public void AddWrongScore()
    {
        wrongTasks++;
        wrongTaskText.text = "FALLOS: " + wrongTasks.ToString();
    }

    public void ShowScorePanel()
    {
        scorePanel.SetActive(true);
    }

    public void HideScorePanel()
    {
        scorePanel.SetActive(false);
    }
}
