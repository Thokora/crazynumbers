﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NoticeController : MonoBehaviour
{
    public Action NoticeDisplayed;

    private float timeToShow = 2f;
    
    [SerializeField] private GameObject noticePanel;
    [SerializeField] private TextAnimationBehaviour noticeText;

    /// <summary>
    /// This method will set number and anfter wil show the panel with number
    /// </summary>
    /// <param name="numberType">Pass number from random number</param>
    public void SetNoticeNumber(string numberName, float timeToShow)
    {
        this.timeToShow = timeToShow;
        noticeText.SetText(numberName);
        noticeText.AnimEnded += OnNoticeEnd;

        StartCoroutine(WaitToShowNoticePanel());
    }

    IEnumerator WaitToShowNoticePanel()
    {
        yield return new WaitForSeconds(timeToShow);
        noticePanel.SetActive(true);
        noticeText.ShowTextAnim(true);
    }

    private void OnNoticeEnd() // when this is called for first time, change the behaviour
    {
        noticeText.AnimEnded -= OnNoticeEnd;
        noticeText.AnimEnded += HideNoticePanel;
        StartCoroutine(WaitForHideNotice());
    }

    IEnumerator WaitForHideNotice()
    {
        yield return new WaitForSeconds(timeToShow);
        noticeText.ShowTextAnim(false);
    }

    private void HideNoticePanel()
    {
        noticeText.AnimEnded -= HideNoticePanel;
        NoticeDisplayed?.Invoke();
        noticePanel.SetActive(false);
    }
}
