﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OptionsController : MonoBehaviour
{
    public Action CorrectResponse;
    public Action WrongResponse;

    [Header("Options Settings")]
    [SerializeField] private int optionAmount;
    [SerializeField] private int maxTryTimes;

    [Space, Header("Response UI elements")]
    [SerializeField] private GameObject reponsesPanel;
    [SerializeField] private TextAnimationBehaviour QuestionTextAnimator;
    [SerializeField] private ResponseButton responseButtonPrefab;
    [SerializeField] private GameObject responseButtonsContainer;
    [SerializeField] private GridLayoutGroup gridLayoutGroup;


    private List<ResponseButton> responseButtons = new List<ResponseButton>();
    private int tryTimes;

    /// <summary>
    /// Use this to set the options according to optionsAmount (variable in editor)
    /// </summary>
    public void SetOptions()
    {
        gridLayoutGroup.enabled = true;
        if (responseButtons.Count == 0)
        {
            for (int i = 0; i < optionAmount; i++)
                AddNewOption();
        }
    }

    /// <summary>
    /// Use this to randomize number options value
    /// </summary>
    /// <param name="correctNumber">this number will be setted in a button as corrected option</param>
    public void OptionRandomizer(Number correctNumber)
    {
        tryTimes = 0;
        gridLayoutGroup.enabled = false;

        // set all buttons with with random Number
        for (int i = 0; i < responseButtons.Count; i++)
            responseButtons[i].SetButtonNumber(NumbersManager.GetRandomNumber(correctNumber).GetNumberValue().ToString(), false); 
        
        //set random button with correct Number
        int optionCorrect = UnityEngine.Random.Range(0, responseButtons.Count);
        responseButtons[optionCorrect].SetButtonNumber(correctNumber.GetNumberValue().ToString(), true);

        SetOptionsVisbility(true);
    }


    private void OnButtonSelected(bool isCorrectOption)
    {
        if (isCorrectOption)
        {
            SetOptionsVisbility(false);
            CorrectResponse?.Invoke();
        }
        else
        {
            tryTimes++;

            if (tryTimes == maxTryTimes)
            {
                for (int i = 0; i < responseButtons.Count; i++)
                    responseButtons[i].RevealResponse();

                tryTimes = 0;

                SetOptionsVisbility(false);
                WrongResponse?.Invoke();
            }
            else
            {
                for (int i = 0; i < responseButtons.Count; i++)
                    responseButtons[i].SwitchInteractable();
            }
        }
    }


    /// <summary>
    /// Use this to add a new responseButton in list
    /// </summary>
    public void AddNewOption()
    {
        ResponseButton responseButtonToAdd = Instantiate(responseButtonPrefab, responseButtonsContainer.transform);
        responseButtonToAdd.OptionSelected += OnButtonSelected;
        responseButtons.Add(responseButtonToAdd);
    }

    /// <summary>
    /// Use this to remove a responseButton from list
    /// </summary>
    public void RemoveAnOption()
    {
        ResponseButton responseButtonToRemove = responseButtons[responseButtons.Count - 1];
        responseButtonToRemove.OptionSelected -= OnButtonSelected;
        responseButtons.Remove(responseButtonToRemove);
        Destroy(responseButtonToRemove);
    }

    private void SetOptionsVisbility(bool show)
    {
        if (QuestionTextAnimator != null)
            QuestionTextAnimator.ShowTextAnim(show);

        for (int i = 0; i < responseButtons.Count; i++)
        {
            responseButtons[i].ShowAnimation(show);
        }
    }

}
