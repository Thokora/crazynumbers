using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AttentionOption : MonoBehaviour
{
    public bool inUse = false;

    [SerializeField] private AttentionElements attentionElement;
    [SerializeField] private List<GameObject> elements = new List<GameObject>();

    private Rigidbody2D rb2D;

    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        DisableAllElements();
    }

    public void SetElementType(int attentionElementID)
    {
        attentionElement = (AttentionElements)attentionElementID;

        elements[attentionElementID].gameObject.SetActive(true);
    }

    public AttentionElements GetAttentionType()
    {
        return attentionElement;
    }

    private void DisableAllElements()
    {
        for (int i = 0; i < elements.Count; i++)
        {
            elements[i].SetActive(false);
        }
    }

    public void SetInitialPos(Vector3 initialPos)
    {
        gameObject.transform.position = initialPos;
        gameObject.SetActive(true);
    }
}
