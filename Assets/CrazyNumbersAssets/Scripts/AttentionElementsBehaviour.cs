using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AttentionElementsBehaviour : MonoBehaviour
{
    public Action TimeFinished;

    [Space] [Header("Game Logic settings")]
    [SerializeField] private float timeToSpawn = 1.5f;
    [SerializeField] private GameObject spawnPosParent;

    private float gameTime;
    private List<AttentionOption> currentAttentionsOptions = new List<AttentionOption>();
    private List<Transform> spawnPos = new List<Transform>();

    private int randomObject;

    private void Awake()
    {
        for (int i = 0; i < spawnPosParent.transform.childCount; i++)
        {
            spawnPos.Add(spawnPosParent.transform.GetChild(i));
        }
    }

    public void SetGameDurationTime( float gameDurationTime)
    {
        gameTime = gameDurationTime;
    }

    public void SetRandomAttentionElements(int totalAttentionObjects, int maxAttentionObjectsPerType, PoolAttentionObjects poolAttentionObjects)
    {
        currentAttentionsOptions.Clear();

        int value = 0;

        while (value < totalAttentionObjects)
        {
            randomObject = UnityEngine.Random.Range(0, maxAttentionObjectsPerType);

            AttentionOption randomAttentionOption = poolAttentionObjects.GetPooledAttentionElement((AttentionElements)randomObject);

            currentAttentionsOptions.Add(randomAttentionOption);
            value++;
        }

        StartCoroutine(SetInitialPos());
    }


    IEnumerator SetInitialPos()
    {
        int posId = 0;

        for (int i = 0; i < currentAttentionsOptions.Count; i++)
        {
            if (posId >= spawnPos.Count)
            {
                posId = 0;
                yield return new WaitForSeconds(timeToSpawn);
            }

            currentAttentionsOptions[i].SetInitialPos(spawnPos[posId].position);
            posId++;
        }

        StartCoroutine(StartGameTime());
    }

    IEnumerator StartGameTime()
    {
        yield return new WaitForSeconds(gameTime);
        TimeFinished?.Invoke();
    }

}
