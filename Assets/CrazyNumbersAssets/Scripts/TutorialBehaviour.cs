﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TutorialBehaviour : MonoBehaviour
{
    public Action StartGame;

    [SerializeField] private GameObject instructionsPanel;
    [SerializeField] private Button startButton;

    private void Awake()
    {
        startButton.onClick.AddListener(OnStartButtonPressed);
        HideInstructionsPanel();
    }

    private void OnStartButtonPressed()
    {
        HideInstructionsPanel();
        StartGame?.Invoke();
    }

    public void ShowInstructionsPanel()
    {
        instructionsPanel.SetActive(true);
    }
    public void HideInstructionsPanel()
    {
        instructionsPanel.SetActive(false);
    }
}
