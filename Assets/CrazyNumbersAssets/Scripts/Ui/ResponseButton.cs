﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

[RequireComponent (typeof(Button))]
[RequireComponent(typeof(Animator))]
public class ResponseButton : MonoBehaviour
{    
    /// <summary>
     /// Bool value refers to option was corrected or was wrong
     /// </summary>
    public Action<bool> OptionSelected;

    private String textInButton = "";
    private bool correctOption;
    private TMP_Text numberText;
    private Button numberButton;
    private Color normalColor;
    private Animator animator;
    private Image image;

    [SerializeField] private Color correctColor;
    [SerializeField] private Color wrongColor;

    
    private void Awake()
    {
        if (transform.GetChild(0).TryGetComponent(out TMP_Text numberText))
            this.numberText = numberText;
        
        numberButton = GetComponent<Button>();
        numberButton.onClick.AddListener(NumberButtonPressed);

        image = GetComponent<Image>();
        normalColor = image.color;

        animator = GetComponent<Animator>();

        ResetButtonState();
    }

    public void SetButtonNumber(string textInButton, bool isCorrectOption)
    {
        ResetButtonState();

        this.textInButton = textInButton;
        correctOption = isCorrectOption;

        if (numberText != null)
            numberText.text = this.textInButton;
    }


    private void NumberButtonPressed()
    {
        RevealResponse();
        OptionSelected?.Invoke(correctOption);
    }

    public void SetButtonColor(Color color)
    {
        image.color = color;
    }

    public void SetButtonColor(bool isCorrectOption)
    {
        if (isCorrectOption)
            image.color = correctColor;
        else
            image.color = wrongColor;
    }

    public void RevealResponse()
    {
        numberButton.interactable = false;
        SetButtonColor(correctOption);
        ShowAnimation(false);
    }

    public void ShowAnimation(bool animationIn)
    {
        animator.SetBool("Show", animationIn);
        SwitchInteractable();
    }

    public void SwitchInteractable()
    {
        StartCoroutine(WaitUntilAnimationEnd());
    }
    IEnumerator WaitUntilAnimationEnd()
    {
        numberButton.interactable = false;
        RuntimeAnimatorController animControlRun = animator.runtimeAnimatorController;

        yield return new WaitForSeconds(animControlRun.animationClips[0].length);
        numberButton.interactable = true;

    }

    private void ResetButtonState()
    {
        SetButtonColor(normalColor);

        textInButton = "";
        correctOption = false;

        if (numberText != null)
            numberText.text = textInButton;

        numberButton.interactable = true;
    }
}
