﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
using TMPro;

public class TextAnimationBehaviour : MonoBehaviour
{
    public Action AnimEnded;

    [SerializeField] private TMP_Text titleText;
    [SerializeField] private Animator animator;

    public void SetText(string textToShow)
    {
        titleText.text = textToShow;
    }

    public void ShowTextAnim(bool show)
    {
        StopAllCoroutines();
        gameObject.SetActive(true);

        if (gameObject.activeInHierarchy)
            StartCoroutine(WaitForAnim(show));
    }

    IEnumerator WaitForAnim(bool show)
    {
        RuntimeAnimatorController animControlRun = animator.runtimeAnimatorController;
        animator.SetBool("Show", show);

        yield return new WaitForSeconds(animControlRun.animationClips[0].length);
        AnimEnded?.Invoke();

        if (!show)
            gameObject.SetActive(false);
        
    }
}
