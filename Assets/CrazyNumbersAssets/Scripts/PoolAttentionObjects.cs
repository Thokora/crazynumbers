using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolAttentionObjects : MonoBehaviour
{
    [SerializeField] private AttentionOption attentionOptionPrefab;
    [SerializeField] private bool willGrow = true;

    private List<List<AttentionOption>> attentionsPooledObjects = new List<List<AttentionOption>>();

    public void Initialize(int elementsTypeAmount, int poolAmountPerElement)
    {
        if (elementsTypeAmount > transform.childCount)
        {
            elementsTypeAmount = transform.childCount;
        }
        else
        {
            for (int i = 0; i < elementsTypeAmount; i++)
            {
                attentionsPooledObjects.Add(new List<AttentionOption>());
            }
        }

        for (int j = 0; j < attentionsPooledObjects.Count; j++)
        {
            for (int i = 0; i < poolAmountPerElement; i++)
            {
                AttentionOption newAttentionObject = Instantiate(attentionOptionPrefab, transform.GetChild(j).transform);
                newAttentionObject.SetElementType(j);
                newAttentionObject.gameObject.SetActive(false);
                attentionsPooledObjects[j].Add(newAttentionObject);
            }
        }
    }
    

    public AttentionOption GetPooledAttentionElement(AttentionElements attentionElementType)
    {
        bool valueReturned = false;
        int attentionElmentId = (int)attentionElementType;

        for (int i = 0; i < attentionsPooledObjects[attentionElmentId].Count; i++)
        {
            if (!valueReturned)
            {
                if (attentionsPooledObjects[attentionElmentId][i] == null)
                {
                    AttentionOption newAttentionObject = Instantiate(attentionOptionPrefab, transform.GetChild(attentionElmentId).transform);
                    newAttentionObject.SetElementType(attentionElmentId);
                    newAttentionObject.gameObject.SetActive(false);

                    attentionsPooledObjects[attentionElmentId][i] = newAttentionObject;
                    newAttentionObject.inUse = true;

                    return attentionsPooledObjects[attentionElmentId][i];
                }

                if (!attentionsPooledObjects[attentionElmentId][i].inUse)
                {
                    valueReturned = true;
                    attentionsPooledObjects[attentionElmentId][i].inUse = true;
                    return attentionsPooledObjects[attentionElmentId][i];
                }
            }
        }

        if (willGrow)
        {
            AttentionOption newAttentionObject = Instantiate(attentionOptionPrefab, transform.GetChild(attentionElmentId).transform);
            attentionsPooledObjects[attentionElmentId].Add(newAttentionObject);
            return newAttentionObject;
        }

        return null;
    }

    public void DisableAllPooledObjs()
    {
        for (int i = 0; i < attentionsPooledObjects.Count; i++)
        {
            for (int j = 0; j < attentionsPooledObjects[i].Count; j++)
            {
                attentionsPooledObjects[i][j].gameObject.SetActive(false);
                attentionsPooledObjects[i][j].inUse = false;
            }
        }
    }
}
