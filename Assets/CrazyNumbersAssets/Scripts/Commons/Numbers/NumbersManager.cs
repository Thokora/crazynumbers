﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class NumbersManager : MonoBehaviour
{
    private static NumbersManager instance;
    private static Number lastRandomNumber = null;
    private static List<Number> Numbers = new List<Number>();

    [Space, Header("Set automatically based on Enums")]
    [Tooltip ("Please, don't forget set numbers in Enums")]
    [SerializeField] private bool autoSetWithEnums = false;

    [Space, Header("Manual numbers setter (RECOMMENDED)")]
    [SerializeField] private AllNumbers allNumbers;
    
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (this != instance)
            Destroy(this.gameObject);

        Numbers.Clear();

        if (!autoSetWithEnums)
        {
            for (int i = 0; i < allNumbers.Numbers.Count; i++)
                Numbers.Add(allNumbers.Numbers[i]);
        }
        else
            SetNumberListAutomatically();
    }

    /// <summary>
    /// Use this to set the numbers list size according to numbersAmount (variable in editor), make sure that you hace all numberType neccesary in Enums
    /// </summary>
    public static void SetNumberListAutomatically()
    {
        var numbersAmount = Enum.GetValues(typeof(NumberType)).Cast<NumberType>().Count();

        if (Numbers.Count == 0)
        {
            for (int i = 0; i < numbersAmount; i++)
            {
                AddNumberToList((NumberType)i);
            }
        }
    }

    /// <summary>
    /// Get a number between 0 and max number in numberList and exclude a specific Number, if this value is null, last randomNumber will be excluded
    /// </summary>
    /// <returns>Number</returns>
    public static Number GetRandomNumber(Number numberToExclude = null)
    {
        if (numberToExclude == null)
            numberToExclude = lastRandomNumber;

        bool randomIsDifferent = false;
        int randomId = -1;

        while (!randomIsDifferent)
        {
            randomId = UnityEngine.Random.Range(0, Numbers.Count);

            if (numberToExclude != Numbers[randomId])
                randomIsDifferent = true;
        }

        lastRandomNumber = Numbers[randomId];

        return lastRandomNumber;
    }

    public static List<Number> GetNumbers()
    {
        return Numbers;
    }

    /// <summary>
    /// Use this to add new number in specific position based o int value
    /// </summary>
    public static void AddNumberToList(int numberValue, string numberName)
    {
        Numbers.Add(new Number(numberValue, numberName));
    }

    /// <summary>
    /// Use this to add new number in specific position based on numberType
    /// </summary>
    public static void AddNumberToList(NumberType numberType)
    {
        Numbers.Add(new Number(numberType));
    }

    /// <summary>
    /// Use this to remove a number in specific position based on int value
    /// </summary>
    public static void RemoveNumberList(int numberValue)
    {
        for (int i = 0; i < Numbers.Count; i++)
        {
            if (Numbers[i].GetNumberValue() == numberValue)
                Numbers.Remove(Numbers[i]);
        }
    }
    /// <summary>
    /// Use this to remove a number in specific position based on numberType
    /// </summary>
    public static void RemoveNumberList(NumberType numberType)
    {
        for (int i = 0; i < Numbers.Count; i++)
        {
            if (Numbers[i].GetNumberType() == numberType)
                Numbers.Remove(Numbers[i]);
        }
    }

}
