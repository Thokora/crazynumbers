﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class AllNumbers
{
    public List<Number> Numbers = new List<Number>();
}
