﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class Number
{
    NumberType numberType;
    [SerializeField] private int numberValue = 0;
    [SerializeField] private string numberName = "";


    /// <summary>
    ///  Use this constructor if you want to create a new number based in int and string
    /// </summary>
    public Number(int numberValue, string numberName)
    {
        this.numberValue = numberValue;
        this.numberName = numberName;

        var numberEnumCount = Enum.GetValues(typeof(NumberType)).Cast<NumberType>().Count();

        if (numberEnumCount > numberValue)
        {
            for (int i = 0; i < numberEnumCount; i++)
            {
                if (i == numberValue)
                    numberType = (NumberType)numberValue;
            }
        }
    }

    /// <summary>
    ///  Use this constructor if you want to create a new number with NumberType, keep in mind you should that you must to add the numberInfo to Enums
    /// </summary>
    public Number(NumberType numberType)
    {
        this.numberType = numberType;
        this.numberValue = (int)numberType;
        this.numberName = numberType.ToString();
    }

    public NumberType GetNumberType()
    {
        return numberType;
    }

    public int GetNumberValue()
    {
        return numberValue;
    }

    public string GetNumberName()
    {
        return numberName;
    }
}
