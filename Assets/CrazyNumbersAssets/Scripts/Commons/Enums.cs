﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Initialization, 
    Instructions, 
    StartGameplay,
    GameLoop,
    EndGameplay
}

public enum NumberType
{
    Cero = 0,
    Uno = 1,
    Dos = 2,
    Tres = 3,
    Cuatro = 4,
    Cinco = 5,
    Seis = 6,
    Siete = 7,
    Ocho = 8,
    Nueve = 9,
}

public enum AttentionElements
{
    Element0 = 0,
    Element1 = 1,
    Element2 = 2,
    Element3 = 3,
    Element4 = 4,
    Element5 = 5,
    Element6 = 6,
    Element7 = 7,
    Element8 = 8
}